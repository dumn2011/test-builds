﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ObjectTarget>
struct Dictionary_2_t2100965753;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>>
struct Dictionary_2_t1322931057;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>>
struct Dictionary_2_t2058017892;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker>
struct Dictionary_2_t858966067;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t3446926030;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t2968050330;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t2728888017;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t365750880;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t619090059;
// System.Func`2<System.Type,Vuforia.Tracker>
struct Func_2_t3173551289;
// System.Func`2<Vuforia.Tracker,System.Boolean>
struct Func_2_t3908638124;
// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor>
struct Func_3_t3440825513;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t3333547397;
// Vuforia.DataSet
struct DataSet_t3286034874;
// Vuforia.ITrackerManager
struct ITrackerManager_t607206903;
// Vuforia.ImageTarget
struct ImageTarget_t3707016494;
// Vuforia.ObjectTracker
struct ObjectTracker_t4177997237;
// Vuforia.StateManager
struct StateManager_t1982749557;
// Vuforia.Trackable
struct Trackable_t2451999991;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// Vuforia.VirtualButton
struct VirtualButton_t386166510;
// Vuforia.VuforiaARController
struct VuforiaARController_t1876945237;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t2151848540;
// Vuforia.WebCam
struct WebCam_t2427002488;




#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ARCONTROLLER_T116632334_H
#define ARCONTROLLER_T116632334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t116632334  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t2151848540 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t116632334, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t2151848540 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t2151848540 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t2151848540 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T116632334_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#define EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManager
struct  EyewearCalibrationProfileManager_t947793426  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGER_T947793426_H
#ifndef EYEWEARUSERCALIBRATOR_T2926839199_H
#define EYEWEARUSERCALIBRATOR_T2926839199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibrator
struct  EyewearUserCalibrator_t2926839199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATOR_T2926839199_H
#ifndef TRACKER_T2709586299_H
#define TRACKER_T2709586299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t2709586299  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t2709586299, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T2709586299_H
#ifndef TRACKERMANAGER_T1703337244_H
#define TRACKERMANAGER_T1703337244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t1703337244  : public RuntimeObject
{
public:
	// Vuforia.StateManager Vuforia.TrackerManager::mStateManager
	StateManager_t1982749557 * ___mStateManager_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager::mTrackers
	Dictionary_2_t858966067 * ___mTrackers_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>> Vuforia.TrackerManager::mTrackerCreators
	Dictionary_2_t1322931057 * ___mTrackerCreators_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>> Vuforia.TrackerManager::mTrackerNativeDeinitializers
	Dictionary_2_t2058017892 * ___mTrackerNativeDeinitializers_4;

public:
	inline static int32_t get_offset_of_mStateManager_1() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mStateManager_1)); }
	inline StateManager_t1982749557 * get_mStateManager_1() const { return ___mStateManager_1; }
	inline StateManager_t1982749557 ** get_address_of_mStateManager_1() { return &___mStateManager_1; }
	inline void set_mStateManager_1(StateManager_t1982749557 * value)
	{
		___mStateManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStateManager_1), value);
	}

	inline static int32_t get_offset_of_mTrackers_2() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackers_2)); }
	inline Dictionary_2_t858966067 * get_mTrackers_2() const { return ___mTrackers_2; }
	inline Dictionary_2_t858966067 ** get_address_of_mTrackers_2() { return &___mTrackers_2; }
	inline void set_mTrackers_2(Dictionary_2_t858966067 * value)
	{
		___mTrackers_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackers_2), value);
	}

	inline static int32_t get_offset_of_mTrackerCreators_3() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackerCreators_3)); }
	inline Dictionary_2_t1322931057 * get_mTrackerCreators_3() const { return ___mTrackerCreators_3; }
	inline Dictionary_2_t1322931057 ** get_address_of_mTrackerCreators_3() { return &___mTrackerCreators_3; }
	inline void set_mTrackerCreators_3(Dictionary_2_t1322931057 * value)
	{
		___mTrackerCreators_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerCreators_3), value);
	}

	inline static int32_t get_offset_of_mTrackerNativeDeinitializers_4() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244, ___mTrackerNativeDeinitializers_4)); }
	inline Dictionary_2_t2058017892 * get_mTrackerNativeDeinitializers_4() const { return ___mTrackerNativeDeinitializers_4; }
	inline Dictionary_2_t2058017892 ** get_address_of_mTrackerNativeDeinitializers_4() { return &___mTrackerNativeDeinitializers_4; }
	inline void set_mTrackerNativeDeinitializers_4(Dictionary_2_t2058017892 * value)
	{
		___mTrackerNativeDeinitializers_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerNativeDeinitializers_4), value);
	}
};

struct TrackerManager_t1703337244_StaticFields
{
public:
	// Vuforia.ITrackerManager Vuforia.TrackerManager::mInstance
	RuntimeObject* ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t1703337244_StaticFields, ___mInstance_0)); }
	inline RuntimeObject* get_mInstance_0() const { return ___mInstance_0; }
	inline RuntimeObject** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(RuntimeObject* value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T1703337244_H
#ifndef U3CU3EC_T1451390621_H
#define U3CU3EC_T1451390621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager/<>c
struct  U3CU3Ec_t1451390621  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1451390621_StaticFields
{
public:
	// Vuforia.TrackerManager/<>c Vuforia.TrackerManager/<>c::<>9
	U3CU3Ec_t1451390621 * ___U3CU3E9_0;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_0
	Func_2_t3173551289 * ___U3CU3E9__8_0_1;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_1
	Func_2_t3173551289 * ___U3CU3E9__8_1_2;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_2
	Func_2_t3908638124 * ___U3CU3E9__8_2_3;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_3
	Func_2_t3908638124 * ___U3CU3E9__8_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1451390621 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1451390621 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1451390621 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t3173551289 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t3173551289 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t3173551289 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t3173551289 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t3173551289 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t3173551289 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_2_3)); }
	inline Func_2_t3908638124 * get_U3CU3E9__8_2_3() const { return ___U3CU3E9__8_2_3; }
	inline Func_2_t3908638124 ** get_address_of_U3CU3E9__8_2_3() { return &___U3CU3E9__8_2_3; }
	inline void set_U3CU3E9__8_2_3(Func_2_t3908638124 * value)
	{
		___U3CU3E9__8_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1451390621_StaticFields, ___U3CU3E9__8_3_4)); }
	inline Func_2_t3908638124 * get_U3CU3E9__8_3_4() const { return ___U3CU3E9__8_3_4; }
	inline Func_2_t3908638124 ** get_address_of_U3CU3E9__8_3_4() { return &___U3CU3E9__8_3_4; }
	inline void set_U3CU3E9__8_3_4(Func_2_t3908638124 * value)
	{
		___U3CU3E9__8_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1451390621_H
#ifndef GLOBALVARS_T2485087241_H
#define GLOBALVARS_T2485087241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities/GlobalVars
struct  GlobalVars_t2485087241  : public RuntimeObject
{
public:

public:
};

struct GlobalVars_t2485087241_StaticFields
{
public:
	// System.String Vuforia.VuforiaRuntimeUtilities/GlobalVars::sUnityAssetsRoot
	String_t* ___sUnityAssetsRoot_1;
	// System.String Vuforia.VuforiaRuntimeUtilities/GlobalVars::EMULATOR_DATABASE_PATH
	String_t* ___EMULATOR_DATABASE_PATH_4;

public:
	inline static int32_t get_offset_of_sUnityAssetsRoot_1() { return static_cast<int32_t>(offsetof(GlobalVars_t2485087241_StaticFields, ___sUnityAssetsRoot_1)); }
	inline String_t* get_sUnityAssetsRoot_1() const { return ___sUnityAssetsRoot_1; }
	inline String_t** get_address_of_sUnityAssetsRoot_1() { return &___sUnityAssetsRoot_1; }
	inline void set_sUnityAssetsRoot_1(String_t* value)
	{
		___sUnityAssetsRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___sUnityAssetsRoot_1), value);
	}

	inline static int32_t get_offset_of_EMULATOR_DATABASE_PATH_4() { return static_cast<int32_t>(offsetof(GlobalVars_t2485087241_StaticFields, ___EMULATOR_DATABASE_PATH_4)); }
	inline String_t* get_EMULATOR_DATABASE_PATH_4() const { return ___EMULATOR_DATABASE_PATH_4; }
	inline String_t** get_address_of_EMULATOR_DATABASE_PATH_4() { return &___EMULATOR_DATABASE_PATH_4; }
	inline void set_EMULATOR_DATABASE_PATH_4(String_t* value)
	{
		___EMULATOR_DATABASE_PATH_4 = value;
		Il2CppCodeGenWriteBarrier((&___EMULATOR_DATABASE_PATH_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALVARS_T2485087241_H
#ifndef U3CU3EC_T3582055403_H
#define U3CU3EC_T3582055403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController/<>c
struct  U3CU3Ec_t3582055403  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3582055403_StaticFields
{
public:
	// Vuforia.WebCamARController/<>c Vuforia.WebCamARController/<>c::<>9
	U3CU3Ec_t3582055403 * ___U3CU3E9_0;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController/<>c::<>9__7_0
	Func_3_t3440825513 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3582055403_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3582055403 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3582055403 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3582055403 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3582055403_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_3_t3440825513 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_3_t3440825513 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_3_t3440825513 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3582055403_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#define __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_t3517759979 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t3517759979__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_T3517759979_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECTANGLEDATA_T1039179782_H
#define RECTANGLEDATA_T1039179782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_t1039179782 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_t1039179782, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_T1039179782_H
#ifndef SIMPLETARGETDATA_T4194873257_H
#define SIMPLETARGETDATA_T4194873257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SimpleTargetData
#pragma pack(push, tp, 1)
struct  SimpleTargetData_t4194873257 
{
public:
	// System.Int32 Vuforia.SimpleTargetData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.SimpleTargetData::unused
	int32_t ___unused_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SimpleTargetData_t4194873257, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_unused_1() { return static_cast<int32_t>(offsetof(SimpleTargetData_t4194873257, ___unused_1)); }
	inline int32_t get_unused_1() const { return ___unused_1; }
	inline int32_t* get_address_of_unused_1() { return &___unused_1; }
	inline void set_unused_1(int32_t value)
	{
		___unused_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETARGETDATA_T4194873257_H
#ifndef TARGETFINDERSTATE_T3286805956_H
#define TARGETFINDERSTATE_T3286805956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetFinderState
#pragma pack(push, tp, 1)
struct  TargetFinderState_t3286805956 
{
public:
	// System.Int32 Vuforia.TargetFinder/TargetFinderState::IsRequesting
	int32_t ___IsRequesting_0;
	// System.Int32 Vuforia.TargetFinder/TargetFinderState::UpdateState
	int32_t ___UpdateState_1;

public:
	inline static int32_t get_offset_of_IsRequesting_0() { return static_cast<int32_t>(offsetof(TargetFinderState_t3286805956, ___IsRequesting_0)); }
	inline int32_t get_IsRequesting_0() const { return ___IsRequesting_0; }
	inline int32_t* get_address_of_IsRequesting_0() { return &___IsRequesting_0; }
	inline void set_IsRequesting_0(int32_t value)
	{
		___IsRequesting_0 = value;
	}

	inline static int32_t get_offset_of_UpdateState_1() { return static_cast<int32_t>(offsetof(TargetFinderState_t3286805956, ___UpdateState_1)); }
	inline int32_t get_UpdateState_1() const { return ___UpdateState_1; }
	inline int32_t* get_address_of_UpdateState_1() { return &___UpdateState_1; }
	inline void set_UpdateState_1(int32_t value)
	{
		___UpdateState_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDERSTATE_T3286805956_H
#ifndef VEC2I_T3527036565_H
#define VEC2I_T3527036565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t3527036565 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T3527036565_H
#ifndef WEBCAMARCONTROLLER_T3718642882_H
#define WEBCAMARCONTROLLER_T3718642882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController
struct  WebCamARController_t3718642882  : public ARController_t116632334
{
public:
	// System.Int32 Vuforia.WebCamARController::RenderTextureLayer
	int32_t ___RenderTextureLayer_1;
	// System.String Vuforia.WebCamARController::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_2;
	// System.Boolean Vuforia.WebCamARController::mFlipHorizontally
	bool ___mFlipHorizontally_3;
	// Vuforia.WebCam Vuforia.WebCamARController::mWebCamImpl
	WebCam_t2427002488 * ___mWebCamImpl_4;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController::mWebCamTexAdaptorProvider
	Func_3_t3440825513 * ___mWebCamTexAdaptorProvider_5;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_1() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___RenderTextureLayer_1)); }
	inline int32_t get_RenderTextureLayer_1() const { return ___RenderTextureLayer_1; }
	inline int32_t* get_address_of_RenderTextureLayer_1() { return &___RenderTextureLayer_1; }
	inline void set_RenderTextureLayer_1(int32_t value)
	{
		___RenderTextureLayer_1 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_2() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mDeviceNameSetInEditor_2)); }
	inline String_t* get_mDeviceNameSetInEditor_2() const { return ___mDeviceNameSetInEditor_2; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_2() { return &___mDeviceNameSetInEditor_2; }
	inline void set_mDeviceNameSetInEditor_2(String_t* value)
	{
		___mDeviceNameSetInEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceNameSetInEditor_2), value);
	}

	inline static int32_t get_offset_of_mFlipHorizontally_3() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mFlipHorizontally_3)); }
	inline bool get_mFlipHorizontally_3() const { return ___mFlipHorizontally_3; }
	inline bool* get_address_of_mFlipHorizontally_3() { return &___mFlipHorizontally_3; }
	inline void set_mFlipHorizontally_3(bool value)
	{
		___mFlipHorizontally_3 = value;
	}

	inline static int32_t get_offset_of_mWebCamImpl_4() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mWebCamImpl_4)); }
	inline WebCam_t2427002488 * get_mWebCamImpl_4() const { return ___mWebCamImpl_4; }
	inline WebCam_t2427002488 ** get_address_of_mWebCamImpl_4() { return &___mWebCamImpl_4; }
	inline void set_mWebCamImpl_4(WebCam_t2427002488 * value)
	{
		___mWebCamImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamImpl_4), value);
	}

	inline static int32_t get_offset_of_mWebCamTexAdaptorProvider_5() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882, ___mWebCamTexAdaptorProvider_5)); }
	inline Func_3_t3440825513 * get_mWebCamTexAdaptorProvider_5() const { return ___mWebCamTexAdaptorProvider_5; }
	inline Func_3_t3440825513 ** get_address_of_mWebCamTexAdaptorProvider_5() { return &___mWebCamTexAdaptorProvider_5; }
	inline void set_mWebCamTexAdaptorProvider_5(Func_3_t3440825513 * value)
	{
		___mWebCamTexAdaptorProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexAdaptorProvider_5), value);
	}
};

struct WebCamARController_t3718642882_StaticFields
{
public:
	// Vuforia.WebCamARController Vuforia.WebCamARController::mInstance
	WebCamARController_t3718642882 * ___mInstance_6;
	// System.Object Vuforia.WebCamARController::mPadlock
	RuntimeObject * ___mPadlock_7;

public:
	inline static int32_t get_offset_of_mInstance_6() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882_StaticFields, ___mInstance_6)); }
	inline WebCamARController_t3718642882 * get_mInstance_6() const { return ___mInstance_6; }
	inline WebCamARController_t3718642882 ** get_address_of_mInstance_6() { return &___mInstance_6; }
	inline void set_mInstance_6(WebCamARController_t3718642882 * value)
	{
		___mInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_6), value);
	}

	inline static int32_t get_offset_of_mPadlock_7() { return static_cast<int32_t>(offsetof(WebCamARController_t3718642882_StaticFields, ___mPadlock_7)); }
	inline RuntimeObject * get_mPadlock_7() const { return ___mPadlock_7; }
	inline RuntimeObject ** get_address_of_mPadlock_7() { return &___mPadlock_7; }
	inline void set_mPadlock_7(RuntimeObject * value)
	{
		___mPadlock_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMARCONTROLLER_T3718642882_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::898C2022A0C02FCE602BF05E1C09BD48301606E5
	__StaticArrayInitTypeSizeU3D24_t3517759979  ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0;

public:
	inline static int32_t get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline __StaticArrayInitTypeSizeU3D24_t3517759979  get_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline __StaticArrayInitTypeSizeU3D24_t3517759979 * get_address_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(__StaticArrayInitTypeSizeU3D24_t3517759979  value)
	{
		___898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef CAMERADEVICEMODE_T2478715656_H
#define CAMERADEVICEMODE_T2478715656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t2478715656 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t2478715656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T2478715656_H
#ifndef CAMERADIRECTION_T637748435_H
#define CAMERADIRECTION_T637748435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t637748435 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t637748435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T637748435_H
#ifndef CLIPPING_MODE_T2655398006_H
#define CLIPPING_MODE_T2655398006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaUtility/CLIPPING_MODE
struct  CLIPPING_MODE_t2655398006 
{
public:
	// System.Int32 Vuforia.HideExcessAreaUtility/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t2655398006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T2655398006_H
#ifndef FRAMEQUALITY_T46289180_H
#define FRAMEQUALITY_T46289180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_t46289180 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameQuality_t46289180, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_T46289180_H
#ifndef TARGETFINDER_T2439332195_H
#define TARGETFINDER_T2439332195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder
struct  TargetFinder_t2439332195  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TargetFinder::mTargetFinderPtr
	intptr_t ___mTargetFinderPtr_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ObjectTarget> Vuforia.TargetFinder::mTargets
	Dictionary_2_t2100965753 * ___mTargets_1;
	// System.IntPtr Vuforia.TargetFinder::mTargetFinderStatePtr
	intptr_t ___mTargetFinderStatePtr_2;
	// Vuforia.TargetFinder/TargetFinderState Vuforia.TargetFinder::mTargetFinderState
	TargetFinderState_t3286805956  ___mTargetFinderState_3;
	// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinder::mNewResults
	List_1_t619090059 * ___mNewResults_4;

public:
	inline static int32_t get_offset_of_mTargetFinderPtr_0() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mTargetFinderPtr_0)); }
	inline intptr_t get_mTargetFinderPtr_0() const { return ___mTargetFinderPtr_0; }
	inline intptr_t* get_address_of_mTargetFinderPtr_0() { return &___mTargetFinderPtr_0; }
	inline void set_mTargetFinderPtr_0(intptr_t value)
	{
		___mTargetFinderPtr_0 = value;
	}

	inline static int32_t get_offset_of_mTargets_1() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mTargets_1)); }
	inline Dictionary_2_t2100965753 * get_mTargets_1() const { return ___mTargets_1; }
	inline Dictionary_2_t2100965753 ** get_address_of_mTargets_1() { return &___mTargets_1; }
	inline void set_mTargets_1(Dictionary_2_t2100965753 * value)
	{
		___mTargets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTargets_1), value);
	}

	inline static int32_t get_offset_of_mTargetFinderStatePtr_2() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mTargetFinderStatePtr_2)); }
	inline intptr_t get_mTargetFinderStatePtr_2() const { return ___mTargetFinderStatePtr_2; }
	inline intptr_t* get_address_of_mTargetFinderStatePtr_2() { return &___mTargetFinderStatePtr_2; }
	inline void set_mTargetFinderStatePtr_2(intptr_t value)
	{
		___mTargetFinderStatePtr_2 = value;
	}

	inline static int32_t get_offset_of_mTargetFinderState_3() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mTargetFinderState_3)); }
	inline TargetFinderState_t3286805956  get_mTargetFinderState_3() const { return ___mTargetFinderState_3; }
	inline TargetFinderState_t3286805956 * get_address_of_mTargetFinderState_3() { return &___mTargetFinderState_3; }
	inline void set_mTargetFinderState_3(TargetFinderState_t3286805956  value)
	{
		___mTargetFinderState_3 = value;
	}

	inline static int32_t get_offset_of_mNewResults_4() { return static_cast<int32_t>(offsetof(TargetFinder_t2439332195, ___mNewResults_4)); }
	inline List_1_t619090059 * get_mNewResults_4() const { return ___mNewResults_4; }
	inline List_1_t619090059 ** get_address_of_mNewResults_4() { return &___mNewResults_4; }
	inline void set_mNewResults_4(List_1_t619090059 * value)
	{
		___mNewResults_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNewResults_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDER_T2439332195_H
#ifndef FILTERMODE_T1400485161_H
#define FILTERMODE_T1400485161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/FilterMode
struct  FilterMode_t1400485161 
{
public:
	// System.Int32 Vuforia.TargetFinder/FilterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterMode_t1400485161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERMODE_T1400485161_H
#ifndef INITSTATE_T538152685_H
#define INITSTATE_T538152685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/InitState
struct  InitState_t538152685 
{
public:
	// System.Int32 Vuforia.TargetFinder/InitState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitState_t538152685, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITSTATE_T538152685_H
#ifndef INTERNALTARGETSEARCHRESULT_T3697474723_H
#define INTERNALTARGETSEARCHRESULT_T3697474723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/InternalTargetSearchResult
#pragma pack(push, tp, 1)
struct  InternalTargetSearchResult_t3697474723 
{
public:
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::TargetNamePtr
	intptr_t ___TargetNamePtr_0;
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::UniqueTargetIdPtr
	intptr_t ___UniqueTargetIdPtr_1;
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::MetaDataPtr
	intptr_t ___MetaDataPtr_2;
	// System.IntPtr Vuforia.TargetFinder/InternalTargetSearchResult::TargetSearchResultPtr
	intptr_t ___TargetSearchResultPtr_3;
	// System.Single Vuforia.TargetFinder/InternalTargetSearchResult::TargetSize
	float ___TargetSize_4;
	// System.Int32 Vuforia.TargetFinder/InternalTargetSearchResult::TrackingRating
	int32_t ___TrackingRating_5;

public:
	inline static int32_t get_offset_of_TargetNamePtr_0() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TargetNamePtr_0)); }
	inline intptr_t get_TargetNamePtr_0() const { return ___TargetNamePtr_0; }
	inline intptr_t* get_address_of_TargetNamePtr_0() { return &___TargetNamePtr_0; }
	inline void set_TargetNamePtr_0(intptr_t value)
	{
		___TargetNamePtr_0 = value;
	}

	inline static int32_t get_offset_of_UniqueTargetIdPtr_1() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___UniqueTargetIdPtr_1)); }
	inline intptr_t get_UniqueTargetIdPtr_1() const { return ___UniqueTargetIdPtr_1; }
	inline intptr_t* get_address_of_UniqueTargetIdPtr_1() { return &___UniqueTargetIdPtr_1; }
	inline void set_UniqueTargetIdPtr_1(intptr_t value)
	{
		___UniqueTargetIdPtr_1 = value;
	}

	inline static int32_t get_offset_of_MetaDataPtr_2() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___MetaDataPtr_2)); }
	inline intptr_t get_MetaDataPtr_2() const { return ___MetaDataPtr_2; }
	inline intptr_t* get_address_of_MetaDataPtr_2() { return &___MetaDataPtr_2; }
	inline void set_MetaDataPtr_2(intptr_t value)
	{
		___MetaDataPtr_2 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_3() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TargetSearchResultPtr_3)); }
	inline intptr_t get_TargetSearchResultPtr_3() const { return ___TargetSearchResultPtr_3; }
	inline intptr_t* get_address_of_TargetSearchResultPtr_3() { return &___TargetSearchResultPtr_3; }
	inline void set_TargetSearchResultPtr_3(intptr_t value)
	{
		___TargetSearchResultPtr_3 = value;
	}

	inline static int32_t get_offset_of_TargetSize_4() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TargetSize_4)); }
	inline float get_TargetSize_4() const { return ___TargetSize_4; }
	inline float* get_address_of_TargetSize_4() { return &___TargetSize_4; }
	inline void set_TargetSize_4(float value)
	{
		___TargetSize_4 = value;
	}

	inline static int32_t get_offset_of_TrackingRating_5() { return static_cast<int32_t>(offsetof(InternalTargetSearchResult_t3697474723, ___TrackingRating_5)); }
	inline int32_t get_TrackingRating_5() const { return ___TrackingRating_5; }
	inline int32_t* get_address_of_TrackingRating_5() { return &___TrackingRating_5; }
	inline void set_TrackingRating_5(int32_t value)
	{
		___TrackingRating_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALTARGETSEARCHRESULT_T3697474723_H
#ifndef TARGETFINDEROBJECTTARGETDATA_T2647159060_H
#define TARGETFINDEROBJECTTARGETDATA_T2647159060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetFinderObjectTargetData
#pragma pack(push, tp, 1)
struct  TargetFinderObjectTargetData_t2647159060 
{
public:
	// System.Int32 Vuforia.TargetFinder/TargetFinderObjectTargetData::id
	int32_t ___id_0;
	// System.Int32 Vuforia.TargetFinder/TargetFinderObjectTargetData::type
	int32_t ___type_1;
	// UnityEngine.Vector3 Vuforia.TargetFinder/TargetFinderObjectTargetData::center
	Vector3_t3722313464  ___center_2;
	// UnityEngine.Vector3 Vuforia.TargetFinder/TargetFinderObjectTargetData::size
	Vector3_t3722313464  ___size_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TargetFinderObjectTargetData_t2647159060, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(TargetFinderObjectTargetData_t2647159060, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(TargetFinderObjectTargetData_t2647159060, ___center_2)); }
	inline Vector3_t3722313464  get_center_2() const { return ___center_2; }
	inline Vector3_t3722313464 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector3_t3722313464  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(TargetFinderObjectTargetData_t2647159060, ___size_3)); }
	inline Vector3_t3722313464  get_size_3() const { return ___size_3; }
	inline Vector3_t3722313464 * get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(Vector3_t3722313464  value)
	{
		___size_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETFINDEROBJECTTARGETDATA_T2647159060_H
#ifndef TARGETSEARCHRESULT_T3441982613_H
#define TARGETSEARCHRESULT_T3441982613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/TargetSearchResult
struct  TargetSearchResult_t3441982613 
{
public:
	// System.String Vuforia.TargetFinder/TargetSearchResult::TargetName
	String_t* ___TargetName_0;
	// System.String Vuforia.TargetFinder/TargetSearchResult::UniqueTargetId
	String_t* ___UniqueTargetId_1;
	// System.Single Vuforia.TargetFinder/TargetSearchResult::TargetSize
	float ___TargetSize_2;
	// System.String Vuforia.TargetFinder/TargetSearchResult::MetaData
	String_t* ___MetaData_3;
	// System.Byte Vuforia.TargetFinder/TargetSearchResult::TrackingRating
	uint8_t ___TrackingRating_4;
	// System.IntPtr Vuforia.TargetFinder/TargetSearchResult::TargetSearchResultPtr
	intptr_t ___TargetSearchResultPtr_5;

public:
	inline static int32_t get_offset_of_TargetName_0() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TargetName_0)); }
	inline String_t* get_TargetName_0() const { return ___TargetName_0; }
	inline String_t** get_address_of_TargetName_0() { return &___TargetName_0; }
	inline void set_TargetName_0(String_t* value)
	{
		___TargetName_0 = value;
		Il2CppCodeGenWriteBarrier((&___TargetName_0), value);
	}

	inline static int32_t get_offset_of_UniqueTargetId_1() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___UniqueTargetId_1)); }
	inline String_t* get_UniqueTargetId_1() const { return ___UniqueTargetId_1; }
	inline String_t** get_address_of_UniqueTargetId_1() { return &___UniqueTargetId_1; }
	inline void set_UniqueTargetId_1(String_t* value)
	{
		___UniqueTargetId_1 = value;
		Il2CppCodeGenWriteBarrier((&___UniqueTargetId_1), value);
	}

	inline static int32_t get_offset_of_TargetSize_2() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TargetSize_2)); }
	inline float get_TargetSize_2() const { return ___TargetSize_2; }
	inline float* get_address_of_TargetSize_2() { return &___TargetSize_2; }
	inline void set_TargetSize_2(float value)
	{
		___TargetSize_2 = value;
	}

	inline static int32_t get_offset_of_MetaData_3() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___MetaData_3)); }
	inline String_t* get_MetaData_3() const { return ___MetaData_3; }
	inline String_t** get_address_of_MetaData_3() { return &___MetaData_3; }
	inline void set_MetaData_3(String_t* value)
	{
		___MetaData_3 = value;
		Il2CppCodeGenWriteBarrier((&___MetaData_3), value);
	}

	inline static int32_t get_offset_of_TrackingRating_4() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TrackingRating_4)); }
	inline uint8_t get_TrackingRating_4() const { return ___TrackingRating_4; }
	inline uint8_t* get_address_of_TrackingRating_4() { return &___TrackingRating_4; }
	inline void set_TrackingRating_4(uint8_t value)
	{
		___TrackingRating_4 = value;
	}

	inline static int32_t get_offset_of_TargetSearchResultPtr_5() { return static_cast<int32_t>(offsetof(TargetSearchResult_t3441982613, ___TargetSearchResultPtr_5)); }
	inline intptr_t get_TargetSearchResultPtr_5() const { return ___TargetSearchResultPtr_5; }
	inline intptr_t* get_address_of_TargetSearchResultPtr_5() { return &___TargetSearchResultPtr_5; }
	inline void set_TargetSearchResultPtr_5(intptr_t value)
	{
		___TargetSearchResultPtr_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t3441982613_marshaled_pinvoke
{
	char* ___TargetName_0;
	char* ___UniqueTargetId_1;
	float ___TargetSize_2;
	char* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
// Native definition for COM marshalling of Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t3441982613_marshaled_com
{
	Il2CppChar* ___TargetName_0;
	Il2CppChar* ___UniqueTargetId_1;
	float ___TargetSize_2;
	Il2CppChar* ___MetaData_3;
	uint8_t ___TrackingRating_4;
	intptr_t ___TargetSearchResultPtr_5;
};
#endif // TARGETSEARCHRESULT_T3441982613_H
#ifndef UPDATESTATE_T1279515537_H
#define UPDATESTATE_T1279515537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TargetFinder/UpdateState
struct  UpdateState_t1279515537 
{
public:
	// System.Int32 Vuforia.TargetFinder/UpdateState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateState_t1279515537, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESTATE_T1279515537_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef STATUSINFO_T1633251416_H
#define STATUSINFO_T1633251416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/StatusInfo
struct  StatusInfo_t1633251416 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/StatusInfo::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StatusInfo_t1633251416, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSINFO_T1633251416_H
#ifndef TRACKABLESOURCE_T2567074243_H
#define TRACKABLESOURCE_T2567074243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableSource
struct  TrackableSource_t2567074243  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.TrackableSource::<TrackableSourcePtr>k__BackingField
	intptr_t ___U3CTrackableSourcePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableSource_t2567074243, ___U3CTrackableSourcePtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CTrackableSourcePtrU3Ek__BackingField_0() const { return ___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CTrackableSourcePtrU3Ek__BackingField_0() { return &___U3CTrackableSourcePtrU3Ek__BackingField_0; }
	inline void set_U3CTrackableSourcePtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CTrackableSourcePtrU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESOURCE_T2567074243_H
#ifndef SENSITIVITY_T3045829715_H
#define SENSITIVITY_T3045829715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton/Sensitivity
struct  Sensitivity_t3045829715 
{
public:
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sensitivity_t3045829715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T3045829715_H
#ifndef RENDEREVENT_T1863578599_H
#define RENDEREVENT_T1863578599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/RenderEvent
struct  RenderEvent_t1863578599 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/RenderEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderEvent_t1863578599, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREVENT_T1863578599_H
#ifndef RENDERERAPI_T402009282_H
#define RENDERERAPI_T402009282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/RendererAPI
struct  RendererAPI_t402009282 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/RendererAPI::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RendererAPI_t402009282, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERAPI_T402009282_H
#ifndef VIDEOTEXTUREINFO_T1805965052_H
#define VIDEOTEXTUREINFO_T1805965052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t1805965052 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t3527036565  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t3527036565  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t1805965052, ___textureSize_0)); }
	inline Vec2I_t3527036565  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t3527036565 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t3527036565  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t1805965052, ___imageSize_1)); }
	inline Vec2I_t3527036565  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t3527036565 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t3527036565  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T1805965052_H
#ifndef INITIALIZABLEBOOL_T3274999204_H
#define INITIALIZABLEBOOL_T3274999204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities/InitializableBool
struct  InitializableBool_t3274999204 
{
public:
	// System.Int32 Vuforia.VuforiaRuntimeUtilities/InitializableBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitializableBool_t3274999204, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEBOOL_T3274999204_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SURFACEUTILITIES_T1841955943_H
#define SURFACEUTILITIES_T1841955943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SurfaceUtilities
struct  SurfaceUtilities_t1841955943  : public RuntimeObject
{
public:

public:
};

struct SurfaceUtilities_t1841955943_StaticFields
{
public:
	// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::mScreenOrientation
	int32_t ___mScreenOrientation_0;

public:
	inline static int32_t get_offset_of_mScreenOrientation_0() { return static_cast<int32_t>(offsetof(SurfaceUtilities_t1841955943_StaticFields, ___mScreenOrientation_0)); }
	inline int32_t get_mScreenOrientation_0() const { return ___mScreenOrientation_0; }
	inline int32_t* get_address_of_mScreenOrientation_0() { return &___mScreenOrientation_0; }
	inline void set_mScreenOrientation_0(int32_t value)
	{
		___mScreenOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEUTILITIES_T1841955943_H
#ifndef VIDEOBACKGROUNDMANAGER_T2198727358_H
#define VIDEOBACKGROUNDMANAGER_T2198727358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_t2198727358  : public ARController_t116632334
{
public:
	// Vuforia.HideExcessAreaUtility/CLIPPING_MODE Vuforia.VideoBackgroundManager::mClippingMode
	int32_t ___mClippingMode_1;
	// UnityEngine.Shader Vuforia.VideoBackgroundManager::mMatteShader
	Shader_t4151988712 * ___mMatteShader_2;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundEnabled
	bool ___mVideoBackgroundEnabled_3;
	// UnityEngine.Texture Vuforia.VideoBackgroundManager::mTexture
	Texture_t3661962703 * ___mTexture_4;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_5;
	// System.IntPtr Vuforia.VideoBackgroundManager::mNativeTexturePtr
	intptr_t ___mNativeTexturePtr_6;

public:
	inline static int32_t get_offset_of_mClippingMode_1() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mClippingMode_1)); }
	inline int32_t get_mClippingMode_1() const { return ___mClippingMode_1; }
	inline int32_t* get_address_of_mClippingMode_1() { return &___mClippingMode_1; }
	inline void set_mClippingMode_1(int32_t value)
	{
		___mClippingMode_1 = value;
	}

	inline static int32_t get_offset_of_mMatteShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mMatteShader_2)); }
	inline Shader_t4151988712 * get_mMatteShader_2() const { return ___mMatteShader_2; }
	inline Shader_t4151988712 ** get_address_of_mMatteShader_2() { return &___mMatteShader_2; }
	inline void set_mMatteShader_2(Shader_t4151988712 * value)
	{
		___mMatteShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundEnabled_3() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mVideoBackgroundEnabled_3)); }
	inline bool get_mVideoBackgroundEnabled_3() const { return ___mVideoBackgroundEnabled_3; }
	inline bool* get_address_of_mVideoBackgroundEnabled_3() { return &___mVideoBackgroundEnabled_3; }
	inline void set_mVideoBackgroundEnabled_3(bool value)
	{
		___mVideoBackgroundEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mTexture_4() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mTexture_4)); }
	inline Texture_t3661962703 * get_mTexture_4() const { return ___mTexture_4; }
	inline Texture_t3661962703 ** get_address_of_mTexture_4() { return &___mTexture_4; }
	inline void set_mTexture_4(Texture_t3661962703 * value)
	{
		___mTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgConfigChanged_5() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mVideoBgConfigChanged_5)); }
	inline bool get_mVideoBgConfigChanged_5() const { return ___mVideoBgConfigChanged_5; }
	inline bool* get_address_of_mVideoBgConfigChanged_5() { return &___mVideoBgConfigChanged_5; }
	inline void set_mVideoBgConfigChanged_5(bool value)
	{
		___mVideoBgConfigChanged_5 = value;
	}

	inline static int32_t get_offset_of_mNativeTexturePtr_6() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358, ___mNativeTexturePtr_6)); }
	inline intptr_t get_mNativeTexturePtr_6() const { return ___mNativeTexturePtr_6; }
	inline intptr_t* get_address_of_mNativeTexturePtr_6() { return &___mNativeTexturePtr_6; }
	inline void set_mNativeTexturePtr_6(intptr_t value)
	{
		___mNativeTexturePtr_6 = value;
	}
};

struct VideoBackgroundManager_t2198727358_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::mInstance
	VideoBackgroundManager_t2198727358 * ___mInstance_7;
	// System.Object Vuforia.VideoBackgroundManager::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358_StaticFields, ___mInstance_7)); }
	inline VideoBackgroundManager_t2198727358 * get_mInstance_7() const { return ___mInstance_7; }
	inline VideoBackgroundManager_t2198727358 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VideoBackgroundManager_t2198727358 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_t2198727358_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDMANAGER_T2198727358_H
#ifndef VIRTUALBUTTON_T386166510_H
#define VIRTUALBUTTON_T386166510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton
struct  VirtualButton_t386166510  : public RuntimeObject
{
public:
	// System.String Vuforia.VirtualButton::mName
	String_t* ___mName_0;
	// System.Int32 Vuforia.VirtualButton::mID
	int32_t ___mID_1;
	// Vuforia.RectangleData Vuforia.VirtualButton::mArea
	RectangleData_t1039179782  ___mArea_2;
	// System.Boolean Vuforia.VirtualButton::mIsEnabled
	bool ___mIsEnabled_3;
	// Vuforia.ImageTarget Vuforia.VirtualButton::mParentImageTarget
	RuntimeObject* ___mParentImageTarget_4;
	// Vuforia.DataSet Vuforia.VirtualButton::mParentDataSet
	DataSet_t3286034874 * ___mParentDataSet_5;

public:
	inline static int32_t get_offset_of_mName_0() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mName_0)); }
	inline String_t* get_mName_0() const { return ___mName_0; }
	inline String_t** get_address_of_mName_0() { return &___mName_0; }
	inline void set_mName_0(String_t* value)
	{
		___mName_0 = value;
		Il2CppCodeGenWriteBarrier((&___mName_0), value);
	}

	inline static int32_t get_offset_of_mID_1() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mID_1)); }
	inline int32_t get_mID_1() const { return ___mID_1; }
	inline int32_t* get_address_of_mID_1() { return &___mID_1; }
	inline void set_mID_1(int32_t value)
	{
		___mID_1 = value;
	}

	inline static int32_t get_offset_of_mArea_2() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mArea_2)); }
	inline RectangleData_t1039179782  get_mArea_2() const { return ___mArea_2; }
	inline RectangleData_t1039179782 * get_address_of_mArea_2() { return &___mArea_2; }
	inline void set_mArea_2(RectangleData_t1039179782  value)
	{
		___mArea_2 = value;
	}

	inline static int32_t get_offset_of_mIsEnabled_3() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mIsEnabled_3)); }
	inline bool get_mIsEnabled_3() const { return ___mIsEnabled_3; }
	inline bool* get_address_of_mIsEnabled_3() { return &___mIsEnabled_3; }
	inline void set_mIsEnabled_3(bool value)
	{
		___mIsEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mParentImageTarget_4() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mParentImageTarget_4)); }
	inline RuntimeObject* get_mParentImageTarget_4() const { return ___mParentImageTarget_4; }
	inline RuntimeObject** get_address_of_mParentImageTarget_4() { return &___mParentImageTarget_4; }
	inline void set_mParentImageTarget_4(RuntimeObject* value)
	{
		___mParentImageTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mParentImageTarget_4), value);
	}

	inline static int32_t get_offset_of_mParentDataSet_5() { return static_cast<int32_t>(offsetof(VirtualButton_t386166510, ___mParentDataSet_5)); }
	inline DataSet_t3286034874 * get_mParentDataSet_5() const { return ___mParentDataSet_5; }
	inline DataSet_t3286034874 ** get_address_of_mParentDataSet_5() { return &___mParentDataSet_5; }
	inline void set_mParentDataSet_5(DataSet_t3286034874 * value)
	{
		___mParentDataSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentDataSet_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T386166510_H
#ifndef VUFORIARUNTIMEUTILITIES_T399660591_H
#define VUFORIARUNTIMEUTILITIES_T399660591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities
struct  VuforiaRuntimeUtilities_t399660591  : public RuntimeObject
{
public:

public:
};

struct VuforiaRuntimeUtilities_t399660591_StaticFields
{
public:
	// Vuforia.VuforiaRuntimeUtilities/InitializableBool Vuforia.VuforiaRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
	// Vuforia.VuforiaRuntimeUtilities/InitializableBool Vuforia.VuforiaRuntimeUtilities::sNativePluginSupport
	int32_t ___sNativePluginSupport_1;

public:
	inline static int32_t get_offset_of_sWebCamUsed_0() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t399660591_StaticFields, ___sWebCamUsed_0)); }
	inline int32_t get_sWebCamUsed_0() const { return ___sWebCamUsed_0; }
	inline int32_t* get_address_of_sWebCamUsed_0() { return &___sWebCamUsed_0; }
	inline void set_sWebCamUsed_0(int32_t value)
	{
		___sWebCamUsed_0 = value;
	}

	inline static int32_t get_offset_of_sNativePluginSupport_1() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t399660591_StaticFields, ___sNativePluginSupport_1)); }
	inline int32_t get_sNativePluginSupport_1() const { return ___sNativePluginSupport_1; }
	inline int32_t* get_address_of_sNativePluginSupport_1() { return &___sNativePluginSupport_1; }
	inline void set_sNativePluginSupport_1(int32_t value)
	{
		___sNativePluginSupport_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIMEUTILITIES_T399660591_H
#ifndef CAMERASTATE_T1646041879_H
#define CAMERASTATE_T1646041879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities/CameraState
struct  CameraState_t1646041879 
{
public:
	// System.Boolean Vuforia.VuforiaRuntimeUtilities/CameraState::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_0;
	// System.Boolean Vuforia.VuforiaRuntimeUtilities/CameraState::<Active>k__BackingField
	bool ___U3CActiveU3Ek__BackingField_1;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaRuntimeUtilities/CameraState::<CameraDirection>k__BackingField
	int32_t ___U3CCameraDirectionU3Ek__BackingField_2;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaRuntimeUtilities/CameraState::<DeviceMode>k__BackingField
	int32_t ___U3CDeviceModeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t1646041879, ___U3CInitializedU3Ek__BackingField_0)); }
	inline bool get_U3CInitializedU3Ek__BackingField_0() const { return ___U3CInitializedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_0() { return &___U3CInitializedU3Ek__BackingField_0; }
	inline void set_U3CInitializedU3Ek__BackingField_0(bool value)
	{
		___U3CInitializedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CActiveU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t1646041879, ___U3CActiveU3Ek__BackingField_1)); }
	inline bool get_U3CActiveU3Ek__BackingField_1() const { return ___U3CActiveU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CActiveU3Ek__BackingField_1() { return &___U3CActiveU3Ek__BackingField_1; }
	inline void set_U3CActiveU3Ek__BackingField_1(bool value)
	{
		___U3CActiveU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCameraDirectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t1646041879, ___U3CCameraDirectionU3Ek__BackingField_2)); }
	inline int32_t get_U3CCameraDirectionU3Ek__BackingField_2() const { return ___U3CCameraDirectionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCameraDirectionU3Ek__BackingField_2() { return &___U3CCameraDirectionU3Ek__BackingField_2; }
	inline void set_U3CCameraDirectionU3Ek__BackingField_2(int32_t value)
	{
		___U3CCameraDirectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDeviceModeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CameraState_t1646041879, ___U3CDeviceModeU3Ek__BackingField_3)); }
	inline int32_t get_U3CDeviceModeU3Ek__BackingField_3() const { return ___U3CDeviceModeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDeviceModeU3Ek__BackingField_3() { return &___U3CDeviceModeU3Ek__BackingField_3; }
	inline void set_U3CDeviceModeU3Ek__BackingField_3(int32_t value)
	{
		___U3CDeviceModeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.VuforiaRuntimeUtilities/CameraState
struct CameraState_t1646041879_marshaled_pinvoke
{
	int32_t ___U3CInitializedU3Ek__BackingField_0;
	int32_t ___U3CActiveU3Ek__BackingField_1;
	int32_t ___U3CCameraDirectionU3Ek__BackingField_2;
	int32_t ___U3CDeviceModeU3Ek__BackingField_3;
};
// Native definition for COM marshalling of Vuforia.VuforiaRuntimeUtilities/CameraState
struct CameraState_t1646041879_marshaled_com
{
	int32_t ___U3CInitializedU3Ek__BackingField_0;
	int32_t ___U3CActiveU3Ek__BackingField_1;
	int32_t ___U3CCameraDirectionU3Ek__BackingField_2;
	int32_t ___U3CDeviceModeU3Ek__BackingField_3;
};
#endif // CAMERASTATE_T1646041879_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef VUFORIAMONOBEHAVIOUR_T1150221792_H
#define VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t1150221792  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T1150221792_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t3109936861  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t3956901511 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t3956901511 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t3956901511 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_t3840446185 * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_t3840446185 * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_t3840446185 * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyStyle_7)); }
	inline GUIStyle_t3956901511 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t3956901511 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerStyle_8)); }
	inline GUIStyle_t3956901511 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t3956901511 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t3956901511 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerStyle_9)); }
	inline GUIStyle_t3956901511 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t3956901511 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t3956901511 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___bodyTexture_10)); }
	inline Texture2D_t3840446185 * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_t3840446185 * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___headerTexture_11)); }
	inline Texture2D_t3840446185 * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_t3840446185 ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_t3840446185 * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t3109936861, ___footerTexture_12)); }
	inline Texture2D_t3840446185 * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_t3840446185 ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_t3840446185 * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T3109936861_H
#ifndef TRACKABLEBEHAVIOUR_T1113559212_H
#define TRACKABLEBEHAVIOUR_T1113559212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t1113559212  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_4;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_5;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_6;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_7;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t3722313464  ___mPreviousScale_8;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_9;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_10;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t2968050330 * ___mTrackableEventHandlers_11;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___U3CTimeStampU3Ek__BackingField_4)); }
	inline double get_U3CTimeStampU3Ek__BackingField_4() const { return ___U3CTimeStampU3Ek__BackingField_4; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_4() { return &___U3CTimeStampU3Ek__BackingField_4; }
	inline void set_U3CTimeStampU3Ek__BackingField_4(double value)
	{
		___U3CTimeStampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableName_5)); }
	inline String_t* get_mTrackableName_5() const { return ___mTrackableName_5; }
	inline String_t** get_address_of_mTrackableName_5() { return &___mTrackableName_5; }
	inline void set_mTrackableName_5(String_t* value)
	{
		___mTrackableName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_5), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreserveChildSize_6)); }
	inline bool get_mPreserveChildSize_6() const { return ___mPreserveChildSize_6; }
	inline bool* get_address_of_mPreserveChildSize_6() { return &___mPreserveChildSize_6; }
	inline void set_mPreserveChildSize_6(bool value)
	{
		___mPreserveChildSize_6 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mInitializedInEditor_7)); }
	inline bool get_mInitializedInEditor_7() const { return ___mInitializedInEditor_7; }
	inline bool* get_address_of_mInitializedInEditor_7() { return &___mInitializedInEditor_7; }
	inline void set_mInitializedInEditor_7(bool value)
	{
		___mInitializedInEditor_7 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreviousScale_8)); }
	inline Vector3_t3722313464  get_mPreviousScale_8() const { return ___mPreviousScale_8; }
	inline Vector3_t3722313464 * get_address_of_mPreviousScale_8() { return &___mPreviousScale_8; }
	inline void set_mPreviousScale_8(Vector3_t3722313464  value)
	{
		___mPreviousScale_8 = value;
	}

	inline static int32_t get_offset_of_mStatus_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mStatus_9)); }
	inline int32_t get_mStatus_9() const { return ___mStatus_9; }
	inline int32_t* get_address_of_mStatus_9() { return &___mStatus_9; }
	inline void set_mStatus_9(int32_t value)
	{
		___mStatus_9 = value;
	}

	inline static int32_t get_offset_of_mTrackable_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackable_10)); }
	inline RuntimeObject* get_mTrackable_10() const { return ___mTrackable_10; }
	inline RuntimeObject** get_address_of_mTrackable_10() { return &___mTrackable_10; }
	inline void set_mTrackable_10(RuntimeObject* value)
	{
		___mTrackable_10 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_10), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableEventHandlers_11)); }
	inline List_1_t2968050330 * get_mTrackableEventHandlers_11() const { return ___mTrackableEventHandlers_11; }
	inline List_1_t2968050330 ** get_address_of_mTrackableEventHandlers_11() { return &___mTrackableEventHandlers_11; }
	inline void set_mTrackableEventHandlers_11(List_1_t2968050330 * value)
	{
		___mTrackableEventHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T1113559212_H
#ifndef USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#define USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t4262637471  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingBehaviour::mObjectTracker
	ObjectTracker_t4177997237 * ___mObjectTracker_4;
	// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.UserDefinedTargetBuildingBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_8;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_10;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingBehaviour::mHandlers
	List_1_t2728888017 * ___mHandlers_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_12;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_13;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_14;

public:
	inline static int32_t get_offset_of_mObjectTracker_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mObjectTracker_4)); }
	inline ObjectTracker_t4177997237 * get_mObjectTracker_4() const { return ___mObjectTracker_4; }
	inline ObjectTracker_t4177997237 ** get_address_of_mObjectTracker_4() { return &___mObjectTracker_4; }
	inline void set_mObjectTracker_4(ObjectTracker_t4177997237 * value)
	{
		___mObjectTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_4), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mLastFrameQuality_5)); }
	inline int32_t get_mLastFrameQuality_5() const { return ___mLastFrameQuality_5; }
	inline int32_t* get_address_of_mLastFrameQuality_5() { return &___mLastFrameQuality_5; }
	inline void set_mLastFrameQuality_5(int32_t value)
	{
		___mLastFrameQuality_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mCurrentlyScanning_6)); }
	inline bool get_mCurrentlyScanning_6() const { return ___mCurrentlyScanning_6; }
	inline bool* get_address_of_mCurrentlyScanning_6() { return &___mCurrentlyScanning_6; }
	inline void set_mCurrentlyScanning_6(bool value)
	{
		___mCurrentlyScanning_6 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mWasScanningBeforeDisable_7)); }
	inline bool get_mWasScanningBeforeDisable_7() const { return ___mWasScanningBeforeDisable_7; }
	inline bool* get_address_of_mWasScanningBeforeDisable_7() { return &___mWasScanningBeforeDisable_7; }
	inline void set_mWasScanningBeforeDisable_7(bool value)
	{
		___mWasScanningBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mCurrentlyBuilding_8)); }
	inline bool get_mCurrentlyBuilding_8() const { return ___mCurrentlyBuilding_8; }
	inline bool* get_address_of_mCurrentlyBuilding_8() { return &___mCurrentlyBuilding_8; }
	inline void set_mCurrentlyBuilding_8(bool value)
	{
		___mCurrentlyBuilding_8 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mWasBuildingBeforeDisable_9)); }
	inline bool get_mWasBuildingBeforeDisable_9() const { return ___mWasBuildingBeforeDisable_9; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_9() { return &___mWasBuildingBeforeDisable_9; }
	inline void set_mWasBuildingBeforeDisable_9(bool value)
	{
		___mWasBuildingBeforeDisable_9 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mOnInitializedCalled_10)); }
	inline bool get_mOnInitializedCalled_10() const { return ___mOnInitializedCalled_10; }
	inline bool* get_address_of_mOnInitializedCalled_10() { return &___mOnInitializedCalled_10; }
	inline void set_mOnInitializedCalled_10(bool value)
	{
		___mOnInitializedCalled_10 = value;
	}

	inline static int32_t get_offset_of_mHandlers_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___mHandlers_11)); }
	inline List_1_t2728888017 * get_mHandlers_11() const { return ___mHandlers_11; }
	inline List_1_t2728888017 ** get_address_of_mHandlers_11() { return &___mHandlers_11; }
	inline void set_mHandlers_11(List_1_t2728888017 * value)
	{
		___mHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_11), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StopTrackerWhileScanning_12)); }
	inline bool get_StopTrackerWhileScanning_12() const { return ___StopTrackerWhileScanning_12; }
	inline bool* get_address_of_StopTrackerWhileScanning_12() { return &___StopTrackerWhileScanning_12; }
	inline void set_StopTrackerWhileScanning_12(bool value)
	{
		___StopTrackerWhileScanning_12 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_13() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StartScanningAutomatically_13)); }
	inline bool get_StartScanningAutomatically_13() const { return ___StartScanningAutomatically_13; }
	inline bool* get_address_of_StartScanningAutomatically_13() { return &___StartScanningAutomatically_13; }
	inline void set_StartScanningAutomatically_13(bool value)
	{
		___StartScanningAutomatically_13 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_14() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t4262637471, ___StopScanningWhenFinshedBuilding_14)); }
	inline bool get_StopScanningWhenFinshedBuilding_14() const { return ___StopScanningWhenFinshedBuilding_14; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_14() { return &___StopScanningWhenFinshedBuilding_14; }
	inline void set_StopScanningWhenFinshedBuilding_14(bool value)
	{
		___StopScanningWhenFinshedBuilding_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGBEHAVIOUR_T4262637471_H
#ifndef VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#define VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundBehaviour
struct  VideoBackgroundBehaviour_t1552899074  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mClearBuffers
	int32_t ___mClearBuffers_4;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_5;
	// Vuforia.VuforiaARController Vuforia.VideoBackgroundBehaviour::mVuforiaARController
	VuforiaARController_t1876945237 * ___mVuforiaARController_6;
	// UnityEngine.Camera Vuforia.VideoBackgroundBehaviour::mCamera
	Camera_t4157153871 * ___mCamera_7;
	// Vuforia.BackgroundPlaneBehaviour Vuforia.VideoBackgroundBehaviour::mBackgroundBehaviour
	BackgroundPlaneBehaviour_t3333547397 * ___mBackgroundBehaviour_8;
	// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer> Vuforia.VideoBackgroundBehaviour::mDisabledMeshRenderers
	HashSet_1_t3446926030 * ___mDisabledMeshRenderers_11;

public:
	inline static int32_t get_offset_of_mClearBuffers_4() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mClearBuffers_4)); }
	inline int32_t get_mClearBuffers_4() const { return ___mClearBuffers_4; }
	inline int32_t* get_address_of_mClearBuffers_4() { return &___mClearBuffers_4; }
	inline void set_mClearBuffers_4(int32_t value)
	{
		___mClearBuffers_4 = value;
	}

	inline static int32_t get_offset_of_mSkipStateUpdates_5() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mSkipStateUpdates_5)); }
	inline int32_t get_mSkipStateUpdates_5() const { return ___mSkipStateUpdates_5; }
	inline int32_t* get_address_of_mSkipStateUpdates_5() { return &___mSkipStateUpdates_5; }
	inline void set_mSkipStateUpdates_5(int32_t value)
	{
		___mSkipStateUpdates_5 = value;
	}

	inline static int32_t get_offset_of_mVuforiaARController_6() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mVuforiaARController_6)); }
	inline VuforiaARController_t1876945237 * get_mVuforiaARController_6() const { return ___mVuforiaARController_6; }
	inline VuforiaARController_t1876945237 ** get_address_of_mVuforiaARController_6() { return &___mVuforiaARController_6; }
	inline void set_mVuforiaARController_6(VuforiaARController_t1876945237 * value)
	{
		___mVuforiaARController_6 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaARController_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mCamera_7)); }
	inline Camera_t4157153871 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t4157153871 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t4157153871 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mBackgroundBehaviour_8() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mBackgroundBehaviour_8)); }
	inline BackgroundPlaneBehaviour_t3333547397 * get_mBackgroundBehaviour_8() const { return ___mBackgroundBehaviour_8; }
	inline BackgroundPlaneBehaviour_t3333547397 ** get_address_of_mBackgroundBehaviour_8() { return &___mBackgroundBehaviour_8; }
	inline void set_mBackgroundBehaviour_8(BackgroundPlaneBehaviour_t3333547397 * value)
	{
		___mBackgroundBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mDisabledMeshRenderers_11() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074, ___mDisabledMeshRenderers_11)); }
	inline HashSet_1_t3446926030 * get_mDisabledMeshRenderers_11() const { return ___mDisabledMeshRenderers_11; }
	inline HashSet_1_t3446926030 ** get_address_of_mDisabledMeshRenderers_11() { return &___mDisabledMeshRenderers_11; }
	inline void set_mDisabledMeshRenderers_11(HashSet_1_t3446926030 * value)
	{
		___mDisabledMeshRenderers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDisabledMeshRenderers_11), value);
	}
};

struct VideoBackgroundBehaviour_t1552899074_StaticFields
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mFrameCounter
	int32_t ___mFrameCounter_9;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mRenderCounter
	int32_t ___mRenderCounter_10;

public:
	inline static int32_t get_offset_of_mFrameCounter_9() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074_StaticFields, ___mFrameCounter_9)); }
	inline int32_t get_mFrameCounter_9() const { return ___mFrameCounter_9; }
	inline int32_t* get_address_of_mFrameCounter_9() { return &___mFrameCounter_9; }
	inline void set_mFrameCounter_9(int32_t value)
	{
		___mFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_mRenderCounter_10() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t1552899074_StaticFields, ___mRenderCounter_10)); }
	inline int32_t get_mRenderCounter_10() const { return ___mRenderCounter_10; }
	inline int32_t* get_address_of_mRenderCounter_10() { return &___mRenderCounter_10; }
	inline void set_mRenderCounter_10(int32_t value)
	{
		___mRenderCounter_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDBEHAVIOUR_T1552899074_H
#ifndef VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#define VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonBehaviour
struct  VirtualButtonBehaviour_t1436326451  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// System.String Vuforia.VirtualButtonBehaviour::mName
	String_t* ___mName_5;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mSensitivity
	int32_t ___mSensitivity_6;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_7;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonBehaviour::mPrevTransform
	Matrix4x4_t1817901843  ___mPrevTransform_8;
	// UnityEngine.GameObject Vuforia.VirtualButtonBehaviour::mPrevParent
	GameObject_t1113636619 * ___mPrevParent_9;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_10;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_11;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_12;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPressed
	bool ___mPressed_13;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonBehaviour::mHandlers
	List_1_t365750880 * ___mHandlers_14;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mLeftTop
	Vector2_t2156229523  ___mLeftTop_15;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mRightBottom
	Vector2_t2156229523  ___mRightBottom_16;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_17;
	// Vuforia.VirtualButton Vuforia.VirtualButtonBehaviour::mVirtualButton
	VirtualButton_t386166510 * ___mVirtualButton_18;

public:
	inline static int32_t get_offset_of_mName_5() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mName_5)); }
	inline String_t* get_mName_5() const { return ___mName_5; }
	inline String_t** get_address_of_mName_5() { return &___mName_5; }
	inline void set_mName_5(String_t* value)
	{
		___mName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mName_5), value);
	}

	inline static int32_t get_offset_of_mSensitivity_6() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mSensitivity_6)); }
	inline int32_t get_mSensitivity_6() const { return ___mSensitivity_6; }
	inline int32_t* get_address_of_mSensitivity_6() { return &___mSensitivity_6; }
	inline void set_mSensitivity_6(int32_t value)
	{
		___mSensitivity_6 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_7() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mHasUpdatedPose_7)); }
	inline bool get_mHasUpdatedPose_7() const { return ___mHasUpdatedPose_7; }
	inline bool* get_address_of_mHasUpdatedPose_7() { return &___mHasUpdatedPose_7; }
	inline void set_mHasUpdatedPose_7(bool value)
	{
		___mHasUpdatedPose_7 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_8() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPrevTransform_8)); }
	inline Matrix4x4_t1817901843  get_mPrevTransform_8() const { return ___mPrevTransform_8; }
	inline Matrix4x4_t1817901843 * get_address_of_mPrevTransform_8() { return &___mPrevTransform_8; }
	inline void set_mPrevTransform_8(Matrix4x4_t1817901843  value)
	{
		___mPrevTransform_8 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_9() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPrevParent_9)); }
	inline GameObject_t1113636619 * get_mPrevParent_9() const { return ___mPrevParent_9; }
	inline GameObject_t1113636619 ** get_address_of_mPrevParent_9() { return &___mPrevParent_9; }
	inline void set_mPrevParent_9(GameObject_t1113636619 * value)
	{
		___mPrevParent_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_9), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_10() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mSensitivityDirty_10)); }
	inline bool get_mSensitivityDirty_10() const { return ___mSensitivityDirty_10; }
	inline bool* get_address_of_mSensitivityDirty_10() { return &___mSensitivityDirty_10; }
	inline void set_mSensitivityDirty_10(bool value)
	{
		___mSensitivityDirty_10 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_11() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPreviousSensitivity_11)); }
	inline int32_t get_mPreviousSensitivity_11() const { return ___mPreviousSensitivity_11; }
	inline int32_t* get_address_of_mPreviousSensitivity_11() { return &___mPreviousSensitivity_11; }
	inline void set_mPreviousSensitivity_11(int32_t value)
	{
		___mPreviousSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_12() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPreviouslyEnabled_12)); }
	inline bool get_mPreviouslyEnabled_12() const { return ___mPreviouslyEnabled_12; }
	inline bool* get_address_of_mPreviouslyEnabled_12() { return &___mPreviouslyEnabled_12; }
	inline void set_mPreviouslyEnabled_12(bool value)
	{
		___mPreviouslyEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mPressed_13() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mPressed_13)); }
	inline bool get_mPressed_13() const { return ___mPressed_13; }
	inline bool* get_address_of_mPressed_13() { return &___mPressed_13; }
	inline void set_mPressed_13(bool value)
	{
		___mPressed_13 = value;
	}

	inline static int32_t get_offset_of_mHandlers_14() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mHandlers_14)); }
	inline List_1_t365750880 * get_mHandlers_14() const { return ___mHandlers_14; }
	inline List_1_t365750880 ** get_address_of_mHandlers_14() { return &___mHandlers_14; }
	inline void set_mHandlers_14(List_1_t365750880 * value)
	{
		___mHandlers_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_14), value);
	}

	inline static int32_t get_offset_of_mLeftTop_15() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mLeftTop_15)); }
	inline Vector2_t2156229523  get_mLeftTop_15() const { return ___mLeftTop_15; }
	inline Vector2_t2156229523 * get_address_of_mLeftTop_15() { return &___mLeftTop_15; }
	inline void set_mLeftTop_15(Vector2_t2156229523  value)
	{
		___mLeftTop_15 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_16() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mRightBottom_16)); }
	inline Vector2_t2156229523  get_mRightBottom_16() const { return ___mRightBottom_16; }
	inline Vector2_t2156229523 * get_address_of_mRightBottom_16() { return &___mRightBottom_16; }
	inline void set_mRightBottom_16(Vector2_t2156229523  value)
	{
		___mRightBottom_16 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_17() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mUnregisterOnDestroy_17)); }
	inline bool get_mUnregisterOnDestroy_17() const { return ___mUnregisterOnDestroy_17; }
	inline bool* get_address_of_mUnregisterOnDestroy_17() { return &___mUnregisterOnDestroy_17; }
	inline void set_mUnregisterOnDestroy_17(bool value)
	{
		___mUnregisterOnDestroy_17 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_18() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_t1436326451, ___mVirtualButton_18)); }
	inline VirtualButton_t386166510 * get_mVirtualButton_18() const { return ___mVirtualButton_18; }
	inline VirtualButton_t386166510 ** get_address_of_mVirtualButton_18() { return &___mVirtualButton_18; }
	inline void set_mVirtualButton_18(VirtualButton_t386166510 * value)
	{
		___mVirtualButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONBEHAVIOUR_T1436326451_H
#ifndef WIREFRAMEBEHAVIOUR_T1831066704_H
#define WIREFRAMEBEHAVIOUR_T1831066704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t1831066704  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// UnityEngine.Material Vuforia.WireframeBehaviour::lineMaterial
	Material_t340375123 * ___lineMaterial_4;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_5;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t2555686324  ___LineColor_6;
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_t340375123 * ___mLineMaterial_7;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___lineMaterial_4)); }
	inline Material_t340375123 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_t340375123 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_t340375123 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_LineColor_6() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___LineColor_6)); }
	inline Color_t2555686324  get_LineColor_6() const { return ___LineColor_6; }
	inline Color_t2555686324 * get_address_of_LineColor_6() { return &___LineColor_6; }
	inline void set_LineColor_6(Color_t2555686324  value)
	{
		___LineColor_6 = value;
	}

	inline static int32_t get_offset_of_mLineMaterial_7() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t1831066704, ___mLineMaterial_7)); }
	inline Material_t340375123 * get_mLineMaterial_7() const { return ___mLineMaterial_7; }
	inline Material_t340375123 ** get_address_of_mLineMaterial_7() { return &___mLineMaterial_7; }
	inline void set_mLineMaterial_7(Material_t340375123 * value)
	{
		___mLineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMEBEHAVIOUR_T1831066704_H
#ifndef WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#define WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_t2143753312  : public VuforiaMonoBehaviour_t1150221792
{
public:
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(WireframeTrackableEventHandler_t2143753312, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMETRACKABLEEVENTHANDLER_T2143753312_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (Vec2I_t3527036565)+ sizeof (RuntimeObject), sizeof(Vec2I_t3527036565 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[2] = 
{
	Vec2I_t3527036565::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2I_t3527036565::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (VideoTextureInfo_t1805965052)+ sizeof (RuntimeObject), sizeof(VideoTextureInfo_t1805965052 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2401[2] = 
{
	VideoTextureInfo_t1805965052::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoTextureInfo_t1805965052::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (RendererAPI_t402009282)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2402[5] = 
{
	RendererAPI_t402009282::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (RenderEvent_t1863578599)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2403[7] = 
{
	RenderEvent_t1863578599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (VuforiaRuntimeUtilities_t399660591), -1, sizeof(VuforiaRuntimeUtilities_t399660591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2404[2] = 
{
	VuforiaRuntimeUtilities_t399660591_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t399660591_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (InitializableBool_t3274999204)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2405[4] = 
{
	InitializableBool_t3274999204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (GlobalVars_t2485087241), -1, sizeof(GlobalVars_t2485087241_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2406[6] = 
{
	0,
	GlobalVars_t2485087241_StaticFields::get_offset_of_sUnityAssetsRoot_1(),
	0,
	0,
	GlobalVars_t2485087241_StaticFields::get_offset_of_EMULATOR_DATABASE_PATH_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (CameraState_t1646041879)+ sizeof (RuntimeObject), sizeof(CameraState_t1646041879_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2407[4] = 
{
	CameraState_t1646041879::get_offset_of_U3CInitializedU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t1646041879::get_offset_of_U3CActiveU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t1646041879::get_offset_of_U3CCameraDirectionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CameraState_t1646041879::get_offset_of_U3CDeviceModeU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (SurfaceUtilities_t1841955943), -1, sizeof(SurfaceUtilities_t1841955943_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2408[1] = 
{
	SurfaceUtilities_t1841955943_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (TargetFinder_t2439332195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[5] = 
{
	TargetFinder_t2439332195::get_offset_of_mTargetFinderPtr_0(),
	TargetFinder_t2439332195::get_offset_of_mTargets_1(),
	TargetFinder_t2439332195::get_offset_of_mTargetFinderStatePtr_2(),
	TargetFinder_t2439332195::get_offset_of_mTargetFinderState_3(),
	TargetFinder_t2439332195::get_offset_of_mNewResults_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (TargetFinderObjectTargetData_t2647159060)+ sizeof (RuntimeObject), sizeof(TargetFinderObjectTargetData_t2647159060 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2410[4] = 
{
	TargetFinderObjectTargetData_t2647159060::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderObjectTargetData_t2647159060::get_offset_of_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderObjectTargetData_t2647159060::get_offset_of_center_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderObjectTargetData_t2647159060::get_offset_of_size_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (InitState_t538152685)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2411[6] = 
{
	InitState_t538152685::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (UpdateState_t1279515537)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2412[12] = 
{
	UpdateState_t1279515537::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (FilterMode_t1400485161)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[3] = 
{
	FilterMode_t1400485161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (TargetSearchResult_t3441982613)+ sizeof (RuntimeObject), sizeof(TargetSearchResult_t3441982613_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[6] = 
{
	TargetSearchResult_t3441982613::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetSearchResult_t3441982613::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (TargetFinderState_t3286805956)+ sizeof (RuntimeObject), sizeof(TargetFinderState_t3286805956 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2415[2] = 
{
	TargetFinderState_t3286805956::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TargetFinderState_t3286805956::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (InternalTargetSearchResult_t3697474723)+ sizeof (RuntimeObject), sizeof(InternalTargetSearchResult_t3697474723 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2416[6] = 
{
	InternalTargetSearchResult_t3697474723::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalTargetSearchResult_t3697474723::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (SimpleTargetData_t4194873257)+ sizeof (RuntimeObject), sizeof(SimpleTargetData_t4194873257 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2417[2] = 
{
	SimpleTargetData_t4194873257::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SimpleTargetData_t4194873257::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (TrackableBehaviour_t1113559212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[8] = 
{
	TrackableBehaviour_t1113559212::get_offset_of_U3CTimeStampU3Ek__BackingField_4(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackableName_5(),
	TrackableBehaviour_t1113559212::get_offset_of_mPreserveChildSize_6(),
	TrackableBehaviour_t1113559212::get_offset_of_mInitializedInEditor_7(),
	TrackableBehaviour_t1113559212::get_offset_of_mPreviousScale_8(),
	TrackableBehaviour_t1113559212::get_offset_of_mStatus_9(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackable_10(),
	TrackableBehaviour_t1113559212::get_offset_of_mTrackableEventHandlers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (Status_t1100905814)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2420[10] = 
{
	Status_t1100905814::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (StatusInfo_t1633251416)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2421[6] = 
{
	StatusInfo_t1633251416::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (TrackableSource_t2567074243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[1] = 
{
	TrackableSource_t2567074243::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (Tracker_t2709586299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2423[1] = 
{
	Tracker_t2709586299::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (TrackerManager_t1703337244), -1, sizeof(TrackerManager_t1703337244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2424[5] = 
{
	TrackerManager_t1703337244_StaticFields::get_offset_of_mInstance_0(),
	TrackerManager_t1703337244::get_offset_of_mStateManager_1(),
	TrackerManager_t1703337244::get_offset_of_mTrackers_2(),
	TrackerManager_t1703337244::get_offset_of_mTrackerCreators_3(),
	TrackerManager_t1703337244::get_offset_of_mTrackerNativeDeinitializers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (U3CU3Ec_t1451390621), -1, sizeof(U3CU3Ec_t1451390621_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2425[5] = 
{
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_2_3(),
	U3CU3Ec_t1451390621_StaticFields::get_offset_of_U3CU3E9__8_3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (UserDefinedTargetBuildingBehaviour_t4262637471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[11] = 
{
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mObjectTracker_4(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mLastFrameQuality_5(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mCurrentlyScanning_6(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mWasScanningBeforeDisable_7(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mCurrentlyBuilding_8(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mWasBuildingBeforeDisable_9(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mOnInitializedCalled_10(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_mHandlers_11(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StopTrackerWhileScanning_12(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StartScanningAutomatically_13(),
	UserDefinedTargetBuildingBehaviour_t4262637471::get_offset_of_StopScanningWhenFinshedBuilding_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (VideoBackgroundBehaviour_t1552899074), -1, sizeof(VideoBackgroundBehaviour_t1552899074_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2428[8] = 
{
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mClearBuffers_4(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mSkipStateUpdates_5(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mVuforiaARController_6(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mCamera_7(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mBackgroundBehaviour_8(),
	VideoBackgroundBehaviour_t1552899074_StaticFields::get_offset_of_mFrameCounter_9(),
	VideoBackgroundBehaviour_t1552899074_StaticFields::get_offset_of_mRenderCounter_10(),
	VideoBackgroundBehaviour_t1552899074::get_offset_of_mDisabledMeshRenderers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (VideoBackgroundManager_t2198727358), -1, sizeof(VideoBackgroundManager_t2198727358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[8] = 
{
	VideoBackgroundManager_t2198727358::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t2198727358::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t2198727358::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t2198727358::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t2198727358::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t2198727358_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (VirtualButton_t386166510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[7] = 
{
	VirtualButton_t386166510::get_offset_of_mName_0(),
	VirtualButton_t386166510::get_offset_of_mID_1(),
	VirtualButton_t386166510::get_offset_of_mArea_2(),
	VirtualButton_t386166510::get_offset_of_mIsEnabled_3(),
	VirtualButton_t386166510::get_offset_of_mParentImageTarget_4(),
	VirtualButton_t386166510::get_offset_of_mParentDataSet_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (Sensitivity_t3045829715)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2431[4] = 
{
	Sensitivity_t3045829715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (VirtualButtonBehaviour_t1436326451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[15] = 
{
	0,
	VirtualButtonBehaviour_t1436326451::get_offset_of_mName_5(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mSensitivity_6(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mHasUpdatedPose_7(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPrevTransform_8(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPrevParent_9(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mSensitivityDirty_10(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPreviousSensitivity_11(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPreviouslyEnabled_12(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mPressed_13(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mHandlers_14(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mLeftTop_15(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mRightBottom_16(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mUnregisterOnDestroy_17(),
	VirtualButtonBehaviour_t1436326451::get_offset_of_mVirtualButton_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (WebCamARController_t3718642882), -1, sizeof(WebCamARController_t3718642882_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2433[7] = 
{
	WebCamARController_t3718642882::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t3718642882::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t3718642882::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t3718642882::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t3718642882::get_offset_of_mWebCamTexAdaptorProvider_5(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mInstance_6(),
	WebCamARController_t3718642882_StaticFields::get_offset_of_mPadlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (U3CU3Ec_t3582055403), -1, sizeof(U3CU3Ec_t3582055403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2434[2] = 
{
	U3CU3Ec_t3582055403_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3582055403_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (EyewearCalibrationProfileManager_t947793426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (EyewearUserCalibrator_t2926839199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (WireframeBehaviour_t1831066704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[4] = 
{
	WireframeBehaviour_t1831066704::get_offset_of_lineMaterial_4(),
	WireframeBehaviour_t1831066704::get_offset_of_ShowLines_5(),
	WireframeBehaviour_t1831066704::get_offset_of_LineColor_6(),
	WireframeBehaviour_t1831066704::get_offset_of_mLineMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (WireframeTrackableEventHandler_t2143753312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2438[1] = 
{
	WireframeTrackableEventHandler_t2143753312::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2439[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (__StaticArrayInitTypeSizeU3D24_t3517759979)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t3517759979 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (DefaultInitializationErrorHandler_t3109936861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[9] = 
{
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t3109936861::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (DefaultTrackableEventHandler_t1588957063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[3] = 
{
	DefaultTrackableEventHandler_t1588957063::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_m_PreviousStatus_5(),
	DefaultTrackableEventHandler_t1588957063::get_offset_of_m_NewStatus_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
